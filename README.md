# React Fundamentals project for Epicodus: Wine Cellar

#### By Jon Stump

* * *

## Description

This is a website for a fictitious wine cellar where they can track their kegs of wine allowing for updates and new kegs to be entered into their inventory using a React page.

* * *

## Technologies used

* React
* Neovim
* HTML
* CSS
* NPM
* Bootstrap
* git
* C8H10N4O2

* * *

## Specs
![Wine Cellar Diagram](/assets/wine_cellar.png)

* * *

## Installation

* Go to ( https://github.com/jonstump/wine-cellar ).

* Navigate to the code button on the github website.

* Click on the code button to open the menu.

- Copy the HTTPS code by clicking the clipboard next to the link.

- Within your Bash terminal navigate to your desired location by using cd fallowed by your desired directory.

```bash
 cd Documents/repos
```

- Once you have chosen your desired directory use the command.

```bash
git clone https://github.com/jonstump/wine-cellar
```

<div
  style="
    background-color: #d1ecf1;
    color: grey; padding: 6px;
    font-size: 9px;
    border-radius: 5px;
    border: 1px solid #d4ecf1;
    margin-bottom: 12px"
>
  <span
    style="
      font-size: 12px;
      font-weight: 600;
      color: #0c5460;"
  >
    ⓘ
  </span>
  <span
    style="
      font-size: 12px;
      font-weight: 900;
      color: #0c5460;
      margin-bottom: 24px"
  >
    Note :
  </span>
  If you have any problems make sure your HTTPS code is correct! The example above might not be the most recent HTTPS code!
</div>

* Now that the project is downloaded you will need to run the command 'npm install' to install all of the packages from the packages.json.

``` bash
npm install
```

* If you would like to see the code use the command "editor ." (where editor is your code editor. example code for VScode and vim for Vim) to open the project in your code editor.

``` bash
nvim .
```

* To launch a local version of the site in your browser enter the following:

``` bash
npm run start
```

This will launch a locally hosted version of the website. You should be able to navigate to http://localhost:3000/ to see the site locally.

This project does not have any sample data stored in it. So if you would like to test functionality you will need to add kegs of wine using the add wine button.

## To dos

* Add warning message that keg is out of pints when someone attempts to sell past zero
* Update a keg to say out of stock at 0 pints
* Add a message for kegs that get low saying "Almost Empty"
* Add some sort of color coding (based on varietal)

## Bugs

* Delete does not delete a keg of wine, it goes back to the wine list

## Resources

* * *

## License
> [GPLv3](/LICENSE)
> Jon Stump &copy; 2021

* * *

## Contact Information

_Jon Stump: [Email](jmstump@gmail.com)_
