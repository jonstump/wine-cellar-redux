import React from "react";
import { v4 } from 'uuid';
import PropTypes from "prop-types";
import ReusableForm from "./ReusableForm";

function NewWineForm(props){
  return (
    <>
      <ReusableForm formSubmissionHandler={handleNewWineFormSubmission}
      buttonText="Add Wine" />
    </>
  );
  function handleNewWineFormSubmission(event) {
    event.preventDefault();
    props.onNewWineCreation({names: event.target.name.value,
                             varietal: event.target.varietal.value,
                             region: event.target.region.value,
                             brand: event.target.brand.value,
                             price: event.target.price.value,
                             pints: event.target.pints = 124,
                             alcoholContent: event.target.alcoholContent.value,
                             id: v4()});
  }
}

NewWineForm.propTypes = {
  onNewWineCreation: PropTypes.func
};

export default NewWineForm;
