import React from "react";
import ReusableForm from "./ReusableForm";
import PropTypes from "prop-types";

function EditWineForm (props) {
  const { wine } = props;

  function handleEditWineFormSubmission(event) {
    event.preventDefault();
    props.onEditWine({name: event.target.name.value,
                      varietal: event.target.varietal.value,
                      region: event.target.region.value,
                      brand: event.target.brand.value,
                      price: event.target.price.value,
                      pints: wine.pints,
                      alcoholContent: event.target.alcoholContent.value,
                      id: wine.id});
  }

  return (
    <>
      <ReusableForm
        formSubmissionHandler={handleEditWineFormSubmission}
        buttonText="Update Wine" />
    </>
  );
}

EditWineForm.propTypes = {
  wine: PropTypes.object,
  onEditWine: PropTypes.func
};

export default EditWineForm;
