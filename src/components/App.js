import React from 'react';
import Header from './Header';
import WineKegControl from './WineKegControl'

function App() {
  return (
    <div className="Wines container">
      <>
        <Header />
        <WineKegControl />
      </>
    </div>
  );
}

export default App;
