import React from "react";
import PropTypes from "prop-types";

function Wine(props){
  return (
    <>
      <div onClick = {() => props.whenWineClicked(props.id)}>
        <h3>{props.name}</h3>
        <p><em>Pints Remaining:{props.pints}</em></p>
        <p>{props.varietal}</p>
        <p>{props.region}</p>
        <p>{props.brand}</p>
        <p>{props.price}</p>
        <p>Alcohol Content:{props.alcoholContent}</p>
        <hr/>
      </div>
    </>
  );
}

Wine.propTypes = {
  name: PropTypes.string,
  varietal: PropTypes.string,
  region: PropTypes.string,
  brand: PropTypes.string,
  price: PropTypes.number,
  pints: PropTypes.number,
  alcoholContent: PropTypes.number,
  id: PropTypes.string,
  whenWineClicked: PropTypes.func
};

export default Wine;
