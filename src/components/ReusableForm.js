import React from "react";
import PropTypes from "prop-types";

function ReusableForm(props) {
  return (
    <>
      <form onSubmit={props.formSubmissionHandler}>
          <div class="row">
            <div class="col">
              <div className="form-group">
                <input className="form-control"
                  type='text'
                  name='name'
                  placeholder='Wine Name' />
              </div>
            </div>
            <div class="col">
              <div className="form-group">
                <input className="form-control"
                  type='text'
                  name='varietal'
                  placeholder='Varietal' />
              </div>
            </div>
            <div class="col">
              <div className="form-group">
                <input className="form-control"
                  type='text'
                  name='region'
                  placeholder='Region wine is from' />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div className="form-group">
                <input className="form-control"
                  type='text'
                  name='brand'
                  placeholder='Wine Maker' />
              </div>
            </div>
            <div class="col">
              <div className="form-group">
                <input className="form-control"
                  type='text'
                  name='price'
                  placeholder='Price' />
              </div>
            </div>
            <div class="col">
              <div className="form-group">
                <input className="form-control"
                  type='text'
                  name='alcoholContent'
                  placeholder='Alcohol Content of Wine' />
              </div>
            </div>
          </div>
        <button className='btn btn-primary' type='submit'>{props.buttonText}</button>
      </form>
    </>
  );
}

ReusableForm.propTypes = {
  formSubmissionHandler: PropTypes.func,
  buttonText: PropTypes.string
};

export default ReusableForm;
